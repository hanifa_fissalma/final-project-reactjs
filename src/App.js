import React, { Component } from 'react';
import './App.css';
import Header from './component/Header';
import Beranda from './pages/Beranda';
import Menu from './pages/Menu';
import MenuMakanan from './pages/MenuMakanan';
import MenuMinuman from './pages/MenuMinuman';
import MenuDetail from './pages/MenuDetail';
import Lokasi from './pages/Lokasi';
import Pesanan from './pages/Pesanan';
import { Provider } from 'react-redux';
import store from './store';
import {BrowserRouter, Route} from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div id="test">
            <Header/>
            <Route exact path="/" component={Beranda}/>
            <Route path="/menu" component={Menu}/>
            <Route path="/makanan" component={MenuMakanan}/>
            <Route path="/minuman" component={MenuMinuman}/>
            <Route path="/menu-detail/:id" component={MenuDetail}/>
            <Route path="/lokasi" component={Lokasi}/>
            <Route path="/pesanan" component={Pesanan}/>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
