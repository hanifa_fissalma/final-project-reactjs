import {alamat} from '../api';

export function fetchAlamat(id){
  return{
    type: "FETCH_ALAMAT",
    payload: alamat.fetchAlamat(id)
  }
}
