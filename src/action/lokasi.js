import {lokasi} from '../api';

export function fetchLokasi(){
  return{
    type: "FETCH_LOKASI",
    payload: lokasi.fetchLokasi()
  }
}
