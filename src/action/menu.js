import {menu} from '../api';

export function fetchMenu(){
  return{
    type: "FETCH_MENU",
    payload: menu.fetchMenu()
  }
}
