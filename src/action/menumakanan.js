import {menumakanan} from '../api';

export function fetchMenuMakanan(){
  return{
    type: "FETCH_MENU_MAKANAN",
    payload: menumakanan.fetchMenuMakanan()
  }
}
