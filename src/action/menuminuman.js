import {menuminuman} from '../api';

export function fetchMenuMinuman(){
  return{
    type: "FETCH_MENU_MINUMAN",
    payload: menuminuman.fetchMenuMinuman()
  }
}
