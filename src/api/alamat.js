import axios from 'axios';
import {service} from '../config';

export const fetchAlamat = (id) => {
    return (dispatch) => {
      axios.get(service+'alamat/'+id)
        .then(response => {
          const alamat = response.data.data
          dispatch({
            type: 'FETCH_ALAMAT',
            payload: {
                alamat: alamat
            }
          })
        }).catch(
            (err)=>{
              console.log(err)
            }
          )
    }
  }
