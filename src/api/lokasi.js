import axios from 'axios';
import {service} from '../config';

export const fetchLokasi = () => {
    return (dispatch) => {
      axios.get(service+'lokasi')
        .then(response => {
          const lokasi = response.data.data
          dispatch({
            type: 'FETCH_LOKASI',
            payload: {
              lokasi: lokasi
            }
          })
        }).catch(
            (err)=>{
              console.log(err)
            }
          )
    }
  }
