import axios from 'axios';
import {service} from '../config';

export const fetchMenu = () => {
    return (dispatch) => {
      axios.get(service+'menu')
        .then(response => {
          const menu = response.data.data
          dispatch({
            type: 'FETCH_MENU',
            payload: {
                menu: menu
            }
          })
        }).catch(
            (err)=>{
              console.log(err)
            }
          )
    }
  }
