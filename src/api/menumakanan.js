import axios from 'axios';
import {service} from '../config';

export const fetchMenuMakanan = () => {
    return (dispatch) => {
      axios.get(service+'menucategory/1')
        .then(response => {
          const menumakanan = response.data.data
          dispatch({
            type: 'FETCH_MENU_MAKANAN',
            payload: {
                menumakanan: menumakanan
            }
          })
        }).catch(
            (err)=>{
              console.log(err)
            }
          )
    }
  }
