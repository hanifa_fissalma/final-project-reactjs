import axios from 'axios';
import {service} from '../config';

export const fetchMenuMinuman = () => {
    return (dispatch) => {
      axios.get(service+'menucategory/2')
        .then(response => {
          const menuminuman = response.data.data
          dispatch({
            type: 'FETCH_MENU_MINUMAN',
            payload: {
                menuminuman: menuminuman
            }
          })
        }).catch(
            (err)=>{
              console.log(err)
            }
          )
    }
  }
