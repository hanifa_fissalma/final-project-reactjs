import React, {Component, Fragment} from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    Button
 } from 'reactstrap';
 import { IoIosCart } from "react-icons/io";
 import { FaSearchLocation } from "react-icons/fa";
 import {FaHome} from 'react-icons/fa';
 import {FaBookOpen} from 'react-icons/fa';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
class Header extends Component{
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false,
          jumlah:''
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    componentDidUpdate(prevProps, prevState){
        if(this.state.jumlah!==prevState.jumlah){
            this.setState({
                jumlah: this.props.pesanan.length
            })
        }
    }
    render(){
        const button={
            margin:5
        }
        return(
            <Fragment>
                <Navbar expand="md" style={{backgroundColor:'#456300'}}>
                    <NavbarBrand href="/">
                        <img src="./logo.png" alt="logo" style={{height:60, paddingBottom:20}}/>  <b style={{color:'#ffcd44', fontSize:30}}>Beli</b> <span style={{color:'#ffe293', fontSize:30}}>Makan</span>
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <Link to="/"><Button color="warning" style={button}><FaHome/> BERANDA</Button></Link> 
                            </NavItem>
                            <NavItem>
                                <Link to="/menu"><Button color="warning" style={button}><FaBookOpen /> MENU</Button> </Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/lokasi"><Button color="warning" style={button}><FaSearchLocation/> LOKASI KAMI</Button> </Link>
                            </NavItem>
                            <NavItem>
                                <Link to="/pesanan"><Button color="warning" style={button}><IoIosCart/> PESANAN {this.state.jumlah < 1 ? null : this.state.jumlah}</Button> </Link>
                            </NavItem>
                        </Nav>
                    </Collapse>
                    </Navbar>
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    pesanan: state.cart.pesanan
  })
export default connect(mapStateToProps,null)(Header)