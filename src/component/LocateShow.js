import React, {Component, Fragment} from 'react';
import './style.css';
class LocateShow extends Component{
    render(){
        const style={
            card:{
                width:'45%',
                margin:10,
                padding:10,
                border:'1px solid grey',
                borderRadius:10,
            }
        }
        const {nama_lokasi, alamat, jam_buka} = this.props
        return(
            <Fragment>
                <div style={style.card} id="box-menu">
                    <b style={{fontSize:16}}>{nama_lokasi}</b>
                    <p style={{fontSize:14}}>{alamat}</p>
                    <p style={{fontSize:14}}>Waktu Buka : Pukul {jam_buka} WIB</p>
                </div>
            </Fragment>
        )
    }
}
export default LocateShow