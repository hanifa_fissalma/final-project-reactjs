import React, {Component, Fragment} from 'react';
import {Card, Button} from 'reactstrap';
import './style.css';

class MenuShow extends Component{
    render(){
        const styleCard={
            width:'100%',
            margin:20,
            padding:10,
            border:0,
            display:'inline-block'
        }
        const styleFoto={
            width:236,
            height:200
        }
        const styleJudul={
            fontWeight:'bold',
            fontSize:16
        }
        const styleHarga={
            fontSize:14
        }
        const {foto, nama_menu, harga} = this.props
        return(
            <Fragment>
                <Card style={styleCard}>
                    <center>
                        <img src={foto} style={styleFoto} alt="foto"/>
                    </center>
                    <center>
                        <span style={styleJudul}>{nama_menu}</span>
                        <br/>
                        <span style={styleHarga}>Rp. {harga}</span>
                        <br/><br/>
                        <Button size="sm" color="danger">PESAN</Button>
                    </center>
                </Card>
            </Fragment>
        )
    }
}
export default MenuShow