import React, {Component, Fragment} from 'react';
import {
    Button,
    ButtonGroup
} from 'reactstrap';
import {Link} from 'react-router-dom';
class SubMenu extends Component{
    render(){
        return(
            <Fragment>
                <div style={{alignSelf:'center'}}>
                    <ButtonGroup size="lg">
                        <Link to="/menu"><Button color="warning">All Menu</Button></Link>
                        <Link to="/makanan"><Button color="warning">Makanan</Button></Link>
                        <Link to="/minuman"><Button color="warning">Minuman</Button></Link>
                    </ButtonGroup>
                </div>
            </Fragment>
        )
    }
}
export default SubMenu