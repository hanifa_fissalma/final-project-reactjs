import React, {Component, Fragment} from 'react';
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption,
    Card,
    Container
} from 'reactstrap';
import Footer from '../component/Footer';
import MenuShow from '../component/MenuShow';
import {connect} from 'react-redux';
import {fetchMenu} from '../api/menu';
import {Route,Link} from 'react-router-dom';
import Loading from './Loading';
const items = [
    {
      src: 'https://cdn.cnn.com/cnnnext/dam/assets/170308101225-fruit-stock-exlarge-169.jpg',
      altText: 'Dipilih dari bahan terbaik',
      caption: 'Dipilih dari bahan terbaik'
    },
    {
      src: 'https://cdn.cnn.com/cnnnext/dam/assets/170308101227-vegetables-stock-exlarge-169.jpg',
      altText: 'Berkualitas juara ',
      caption: 'Berkualitas juara'
    },
    {
      src: 'https://cdn.cnn.com/cnnnext/dam/assets/170308101229-nuts-seeds-stock-exlarge-169.jpg',
      altText: 'Diolah dengan sempurna',
      caption: 'Diolah dengan sempurna'
    }
];
class Beranda extends Component{
    constructor(props) {
        super(props);
        this.state = { 
            activeIndex: 0,
            menu:[]
        };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }
    componentDidMount(){
        this.props.fetchMenu();
    }
    onExiting() {
        this.animating = true;
    }
    
    onExited() {
        this.animating = false;
    }
    
    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }
    
    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }
    
    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }
    render(){
        const style={
            margin:20,
            padding: 20,
            display:'flex',
            flexWrap:'wrap',
            flexDirection:'row'
        }
        const styleContainer={
            padding: 20, 
            marginTop:10,
            backgroundColor:'white',
        }
        const { activeIndex } = this.state;
        const slides = items.map((item) => {
            return (
                <CarouselItem
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                >
                <img src={item.src} alt={item.altText} />
                <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
                </CarouselItem>
             );
        });
        const {menu} = this.props
        return(
            <Fragment>
                <Container style={styleContainer}>
                    <div style={{margin:50, paddingLeft:100}}>
                        <Carousel
                            activeIndex={activeIndex}
                            next={this.next}
                            previous={this.previous}
                        >
                            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                            {slides}
                            <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                            <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
                        </Carousel>
                    </div>
                    <center><h3>REKOMENDASI</h3></center>
                    <br/>
                    {menu < 1 ? <Loading/> :
                        <Card style={style}>
                            {menu.slice(5,10).map((menu, index)=>
                                <Route key={index}>
                                    <Link to={`menu-detail/${menu.id_menu}`} style={{color:'black'}}>
                                        <MenuShow 
                                            key={menu.id_menu}
                                            foto={menu.foto}
                                            nama_menu={menu.nama_menu}
                                            harga={menu.harga}
                                        />
                                    </Link>
                                </Route>
                            )}
                        </Card>
                    }
                </Container>
                <Footer/>
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    menu: state.menu.menu
  })
  
const mapDispatchToProps = dispatch => ({
    fetchMenu: () => dispatch(fetchMenu())
  })
  
export default connect(mapStateToProps, mapDispatchToProps)(Beranda)