import React from 'react';
import { BeatLoader } from 'react-spinners';
 
class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }
  render() {
    return (
      <div className='sweet-loading'>
        <center>
            <BeatLoader
                sizeUnit={"px"}
                size={20}
                color={'#078331'}
                loading={this.state.loading}
            />
        </center>
      </div> 
    )
  }
}
export default Loading;