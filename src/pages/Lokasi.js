import React, {Component, Fragment} from 'react';
import {Card, Container} from 'reactstrap';
import LocateShow from '../component/LocateShow';
import {connect} from 'react-redux';
import {fetchLokasi} from '../api/lokasi';
import Loading from './Loading';
class Lokasi extends Component{
    constructor(props){
        super(props);
        this.state={
            lokasi:[]
        };
    }
    componentDidMount(){
        this.props.fetchLokasi()
    }
    render(){
        const style={
            margin: 10,
            padding:20,
            borderRadius:20,
            display: 'flex',
            flexWrap: 'wrap',
            width:'100%',
            flexDirection:'row'
        }
        const {lokasi} = this.props;
        return(
            <Fragment>
                <Container>
                    <center style={{margin:30}}><h3>Lokasi Kami</h3></center>
                    {lokasi < 1 ? <Loading/> : 
                        <Card style={style}>
                        { lokasi.map((lokasi, index) =>
                            <LocateShow 
                                key={index}
                                nama_lokasi={lokasi.nama}
                                alamat={lokasi.alamat}
                                jam_buka={lokasi.jam_buka}
                            />
                        )}
                    </Card>
                    }
                </Container>
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    lokasi: state.lokasi.lokasi
  })
  
const mapDispatchToProps = dispatch => ({
    fetchLokasi: () => dispatch(fetchLokasi())
  })
  
export default connect(mapStateToProps, mapDispatchToProps)(Lokasi)