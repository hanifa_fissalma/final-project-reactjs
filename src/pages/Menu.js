import React, {Component, Fragment} from 'react';
import MenuShow from '../component/MenuShow';
import SubMenu from '../component/SubMenu';
import {Card, Container} from 'reactstrap';
import {connect} from 'react-redux';
import {fetchMenu} from '../api/menu';
import {Route, Link} from 'react-router-dom';
import Loading from './Loading';
class Menu extends Component{
    constructor(props){
        super(props);
        this.state={
            menu:[]
        }
    }
    componentDidMount(){
        this.props.fetchMenu();
    }
    render(){
        const style={
            padding: 20, 
            marginTop:10,
            backgroundColor:'white',
            display:'flex',
            flexWrap:'wrap',
            flexDirection:'row'
        }
        const {menu} = this.props
        return(
            <Fragment>
                <Container>
                    <center style={{margin:30}}><h3>Menu</h3></center>
                    <center><SubMenu/></center>
                    <br/><br/>
                    {menu < 1  ? <Loading/> : 
                        <Card style={style}>
                            {menu.map((menu, index)=>
                                <Route key={index}>
                                    <Link to={`menu-detail/${menu.id_menu}`} style={{color:'black', }}>
                                        <MenuShow 
                                            key={menu.menu_id}
                                            foto={menu.foto}
                                            nama_menu={menu.nama_menu}
                                            harga={menu.harga}
                                        />  
                                    </Link>
                                </Route>
                            )}
                        </Card>
                    }
                </Container>
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    menu: state.menu.menu
  })
  
const mapDispatchToProps = dispatch => ({
    fetchMenu: () => dispatch(fetchMenu())
  })
  
export default connect(mapStateToProps, mapDispatchToProps)(Menu)