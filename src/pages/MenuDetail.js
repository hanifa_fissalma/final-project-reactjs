import React, {Component,Fragment} from 'react';
import{
    Card,
    Row,
    Col,
    Table,
    Button,
} from 'reactstrap';
import {Link} from 'react-router-dom';
import axios from 'axios';
import {service} from '../config';
import {getPesanan} from '../action/cart';
import {connect} from 'react-redux';
class MenuDetail extends Component{
    constructor(props){
        super(props);
        this.state={
            menu:[],
            pesanan:[]
        }
    }
    componentDidMount(){
        const { match } = this.props
        axios.get(service+'menu/'+match.params.id)
            .then( ({ data }) => {
                this.setState({
                    menu: data.data[0]
                })
        })
        // this.props.getPesanan(this.state.menu.id_menu);
    }
    handleClick(){
        this.props.getPesanan(this.state.menu.id_menu)
    }
    render(){
        const style={
            margin: 30,
            padding:20,
            borderRadius:20
        }
        const {menu} = this.state;
        return(
            <Fragment>
                <center style={{margin:30}}><h3>Menu</h3></center>
                <Card style={style}>
                    <Row>
                        <Col sm={12} md={3}>
                            <Link to="/menu"> Kembali ke Menu Utama </Link>
                            <br/>
                            <img src={menu.foto} alt="foto-menu" style={{width:300, height:300, float:'left'}}/>
                        </Col>
                        <Col sm={12} md={9}>
                            <Table>
                                <thead>
                                    <tr>
                                        <td style={{fontSize:18}}>Nama</td>
                                        <td style={{fontSize:18}}><b>{menu.nama_menu}</b></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style={{fontSize:18}}>Kandungan</td>
                                        <td style={{fontSize:18}}>{menu.kandungan}</td>
                                    </tr>
                                    <tr>
                                        <td style={{fontSize:18}}>Komposisi</td>
                                        <td style={{fontSize:18}}>{menu.deskripsi_menu}</td>
                                    </tr>
                                    <tr>
                                        <td style={{fontSize:18}}>Harga</td>
                                        <td style={{fontSize:18}}>
                                            Rp.{menu.harga}  
                                            <br/><br/>
                                            <Link to="/menu"><Button color="danger" onClick={this.props.getPesanan(menu.id_menu)}>PESAN</Button></Link>
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Card>
            </Fragment>
        )
    }
}
// const mapStateToProps = state => ({
//     pesanan: state.pesanan.pesanan
//   })
  
const mapDispatchToProps = dispatch => ({
    getPesanan: (id) => dispatch(getPesanan(id))
})
  
export default connect(null, mapDispatchToProps)(MenuDetail)