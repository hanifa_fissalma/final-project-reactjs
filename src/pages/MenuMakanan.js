import React, {Component, Fragment} from 'react';
import MenuShow from '../component/MenuShow';
import SubMenu from '../component/SubMenu';
import {Card,Container} from 'reactstrap';
import {connect} from 'react-redux';
import {fetchMenuMakanan} from '../api/menumakanan';
import {Route,Link} from 'react-router-dom';
import Loading from './Loading';
class MenuMakanan extends Component{
    constructor(props){
        super(props);
        this.state={
            menumakanan:[]
        }
    }
    componentDidMount(){
        this.props.fetchMenuMakanan();
    }
    render(){
        const style={
            padding: 20, 
            marginTop:10,
            backgroundColor:'white',
            display:'flex',
            flexWrap:'wrap',
            flexDirection:'row'
        }
        const {menumakanan} = this.props
        return(
            <Fragment>
                <Container>
                    <center style={{margin:30}}><h3>Menu - Makanan</h3></center>
                    <br/><br/>
                    <center><SubMenu/></center>
                    {menumakanan < 1 ? <Loading/> : 
                        <Card style={style}>
                            {menumakanan.map((menumakanan, index)=>
                                <Route key={index}>
                                    <Link to={`menu-detail/${menumakanan.id_menu}`} style={{color:'black'}}>
                                        <MenuShow 
                                            key={menumakanan.id_menu}
                                            foto={menumakanan.foto}
                                            nama_menu={menumakanan.nama_menu}
                                            harga={menumakanan.harga}
                                        />  
                                    </Link>
                                </Route>
                            )}
                        </Card>
                    }
                </Container>
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    menumakanan: state.menumakanan.menumakanan
  })
  
const mapDispatchToProps = dispatch => ({
    fetchMenuMakanan: () => dispatch(fetchMenuMakanan())
  })
  
export default connect(mapStateToProps, mapDispatchToProps)(MenuMakanan)