import React, {Component, Fragment} from 'react';
import MenuShow from '../component/MenuShow';
import SubMenu from '../component/SubMenu';
import {Card, Container} from 'reactstrap';
import {connect} from 'react-redux';
import {fetchMenuMinuman} from '../api/menuminuman';
import {Route, Link} from 'react-router-dom';
import Loading from './Loading';
class MenuMinuman extends Component{
    constructor(props){
        super(props);
        this.state={
            menuminuman:[]
        }
    }
    componentDidMount(){
        this.props.fetchMenuMinuman();
    }
    render(){
        const style={
            padding: 20, 
            marginTop:10,
            backgroundColor:'white',
            display:'flex',
            flexWrap:'wrap',
            flexDirection:'row'
        };
        const {menuminuman}=this.props;
        return(
            <Fragment>
                <Container>
                    <center style={{margin:30}}><h3>Menu - Minuman</h3></center>
                    <center><SubMenu/></center>
                    <br/><br/>
                    {menuminuman < 1 ? <Loading/> : 
                        <Card style={style}>
                            {menuminuman.map((menuminuman, index)=>
                                <Route key={index}>
                                    <Link to={`menu-detail/${menuminuman.id_menu}`} style={{color:'black'}}>
                                        <MenuShow 
                                            key={menuminuman.id_menu}
                                            foto={menuminuman.foto}
                                            nama_menu={menuminuman.nama_menu}
                                            harga={menuminuman.harga}
                                        />  
                                    </Link>
                                </Route>
                            )}
                        </Card>
                    }
                </Container>
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    menuminuman: state.menuminuman.menuminuman
  })
  
const mapDispatchToProps = dispatch => ({
    fetchMenuMinuman: () => dispatch(fetchMenuMinuman())
  })
  
export default connect(mapStateToProps, mapDispatchToProps)(MenuMinuman)