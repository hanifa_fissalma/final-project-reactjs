import React, {Component, Fragment} from 'react';
import { 
    Table,
    Card
} from 'reactstrap';
import {connect} from 'react-redux';
import axios from 'axios';
import {service} from '../config';
class Pesanan extends Component{
    constructor(props){
        super(props);
        this.state={
            data_pesanan:[]
        }
    }
    componentDidMount(){
        const { pesanan } = this.props
        axios.get(service+'menu/'+pesanan)
            .then( ({ data }) => {
                this.setState({
                    data_pesanan: data.data
                })
        })
    }
    render(){
        const style={
            margin: 30,
            padding:20,
            borderRadius:20
        }
        const {data_pesanan} = this.state;
        return(
            <Fragment>
                <center style={{margin:30}}><h3>Konfirmasi Pemesanan</h3></center>
                {this.state.data_pesanan < 1 ? <center>Tidak ada pesanan</center> : 
                    data_pesanan.map((data_pesanan)=>
                        <Card style={style} key={data_pesanan.id_menu}>
                            <br/>
                            <br/>
                            <div style={{marginRight:100, marginLeft:100}}>
                                <Table>
                                    <thead>
                                        <tr>
                                            <td style={{fontSize:20}}>Pesanan Anda</td>
                                            <td style={{fontSize:20}}>Jumlah</td>
                                            <td style={{fontSize:20}}>Harga</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <img src={data_pesanan.foto} alt="foto-menu" style={{width:138, height:110}}/>
                                                &nbsp;&nbsp;&nbsp;
                                                <b style={{fontSize:16}}>{data_pesanan.nama_menu}</b>
                                            </td>
                                            <td>
                                                1
                                            </td>
                                            <td>
                                                <b style={{fontSize:16}}>Rp. {data_pesanan.harga} </b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style={{fontSize:16}}>
                                                <b>Total</b>
                                            </td>
                                            <td>
                                                <b style={{fontSize:16}}>Rp. {data_pesanan.harga}</b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </div>
                        </Card>
                    )
                }
            </Fragment>
        )
    }
}
const mapStateToProps = state => ({
    pesanan: state.cart.pesanan
  })
export default connect(mapStateToProps,null)(Pesanan)