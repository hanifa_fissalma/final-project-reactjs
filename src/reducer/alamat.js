const initialState = {
    alamat:[]
  }
  
  const alamatReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_ALAMAT':
        return {
          ...state,
          alamat: action.payload.alamat,
        }
  
      default:
        return state
    }
  }
  
  export default alamatReducer