const initialState = {
    pesanan:[]
  }
  
const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_PESANAN':
            return {
            ...state,
            pesanan: [...state.pesanan, action.payload],
            }

        default:
            return state
    }
}

export default cartReducer