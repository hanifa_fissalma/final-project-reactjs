const initialState = {
    lokasi:[]
  }
  
  const lokasiReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_LOKASI':
        return {
          ...state,
          lokasi: action.payload.lokasi,
        }
  
      default:
        return state
    }
  }
  
  export default lokasiReducer