const initialState = {
    menu:[]
  }
  
  const menuReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_MENU':
        return {
          ...state,
          menu: action.payload.menu,
        }
  
      default:
        return state
    }
  }
  
  export default menuReducer