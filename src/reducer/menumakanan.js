const initialState = {
    menumakanan:[]
  }
  
  const menumakananReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_MENU_MAKANAN':
        return {
          ...state,
          menumakanan: action.payload.menumakanan,
        }
  
      default:
        return state
    }
  }
  
  export default menumakananReducer