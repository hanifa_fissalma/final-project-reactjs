const initialState = {
    menuminuman:[]
  }
  
  const menuminumanReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'FETCH_MENU_MINUMAN':
        return {
          ...state,
          menuminuman: action.payload.menuminuman,
        }
  
      default:
        return state
    }
  }
  
  export default menuminumanReducer