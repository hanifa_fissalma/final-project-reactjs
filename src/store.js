import {createStore, combineReducers, applyMiddleware} from 'redux';
import lokasiReducer from './reducer/lokasi';
import menuReducer from './reducer/menu';
import menumakananReducer from './reducer/menumakanan';
import menuminumanReducer from './reducer/menuminuman';
import cartReducer from './reducer/cart';
import alamatReducer from './reducer/alamat';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
const reducers = combineReducers({
    lokasi: lokasiReducer,
    menu: menuReducer,
    menumakanan: menumakananReducer,
    menuminuman: menuminumanReducer,
    cart: cartReducer,
    alamat:alamatReducer,

})
const middleware = applyMiddleware(logger, thunk)
export default createStore(reducers, middleware)